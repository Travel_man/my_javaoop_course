import java.util.Scanner;

public class HouseMain {

	public static void main(String[] args) {
		int appNumber;
		int totEntrance = 4;
		int app = 4;
		int totFloor = 9;
		double floor;
		Scanner sc = new Scanner(System.in);
		System.out.println("Number of appartment, please");
		appNumber = sc.nextInt();

		int totApp = app * totFloor * totEntrance;
		System.out.println("Total app " + totApp);

		if (appNumber >= 0 && appNumber <= (totApp / totEntrance)) {
			floor = totFloor - (((app * totFloor) - appNumber) / 4);
			if (floor < 1) {
				floor++;
			}
			System.out.println("Appartment " + appNumber + " " + " is at  entrance " + (totEntrance / totEntrance)
					+ " on " + floor + " floor.");
		}

		else if (appNumber >= 0 && appNumber >= (totApp / totEntrance) && appNumber <= (totApp / totEntrance) * 2) {
			floor = totFloor - (((2 * app * totFloor) - appNumber) / 4);
			if (floor < 1) {
				floor++;
			}
			System.out.println("Appartment " + appNumber + " " + " is at  entrance " + (totEntrance / totEntrance * 2)
					+ " on " + floor + " floor.");
		}

		else if (appNumber >= 0 && appNumber >= (totApp / totEntrance) && appNumber >= (totApp / totEntrance) * 2
				&& appNumber <= (totApp / totEntrance) * 3) {
			floor = totFloor - (((3 * app * totFloor) - appNumber) / 4);
			if (floor < 1) {
				floor++;
			}
			System.out.println("Appartment " + appNumber + " " + " is at  entrance " + (totEntrance / totEntrance * 3)
					+ " on " + floor + " floor.");
		}

		else if (appNumber >= 0 && appNumber >= (totApp / totEntrance) && appNumber >= (totApp / totEntrance) * 2
				&& appNumber >= (totApp / totEntrance) * 3 && appNumber <= (totApp / totEntrance) * 4) {
			floor = totFloor - (((4 * app * totFloor) - appNumber) / 4);
			if (floor < 1) {
				floor++;
			}
			System.out.println("Appartment " + appNumber + " " + " is at  entrance " + (totEntrance / totEntrance * 4)
					+ " on " + floor + " floor.");
		}

		else {
			System.out.println("This appartment number have not in this house!");
		}
	}
}