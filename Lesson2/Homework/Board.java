import java.util.Arrays;

public class Board {

	private Shape[] board;

	public Board() {
		this.board = new Shape[4];
	}

	public void addShape(Shape shape, int part) {
		boolean avalible = false;
		int identify = 0;
		if (part <= 0 || part > 4) {
			System.out.println("Часть доски указана не верно. Проверьте правильность данных!");

		}
		for (int i = 0; i < board.length; i++) {
			if (board[i] == shape) {
				avalible = true;
				break;
			}
		}
		if (avalible)
			System.out.println(" Фигура на доске, в  " + identify + 1 + " части.Фигуру нельзя добавить дважды");
		else {
			if (board[part - 1] != null)
				delShape(shape);
			board[part - 1] = shape;

		}
	}

	public void delShape(Shape shape) {
		for (int i = 0; i < board.length; i++) {
			if (board[i] == shape)
				board[i] = null;
		}
	}

	public void getShapesInfo() {
		System.out.println("На доске есть такие фигуры: ");
		double totalArea = 0;
		for (int i = 0; i < board.length; i++) {
			if (board[i] != null) {
				System.out.println("Чaсть " + (i + 1) + ": " + board[i]);
				totalArea += board[i].getArea();
			} else
				System.out.println("Часть " + (i + 1) + " не заполнена.");
		}
		System.out.println("Общая площадь фигур: " + totalArea);
	}

	@Override
	public String toString() {
		return "Board [board=" + Arrays.toString(board) + ", toString()=" + super.toString() + "]";
	}
}