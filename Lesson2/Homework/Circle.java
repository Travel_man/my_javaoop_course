public class Circle extends Shape {

	private Point centr;
	private Point radius;

	public Circle(Point centr, Point radius) {

		super();
		this.centr = centr;
		this.radius = radius;
	}

	public Circle() {
		super();
	}

	public Point getCentr() {
		return centr;
	}

	public void setCentr(Point centr) {
		this.centr = centr;
	}

	public Point getRadius() {
		return radius;
	}

	public void setRadius(Point radius) {
		this.radius = radius;
	}

	@Override
	double getPerimetr() {
		double perimetr = 2 * Math.PI * centr.calcDistance(radius);
		return perimetr;
	}

	@Override
	double getArea() {
		double area = Math.pow(centr.calcDistance(radius), 2);
		return area;
	}

	@Override
	public String toString() {
		return "Circle [getPerimetr()=" + getPerimetr() + ", getArea()=" + getArea() + "]";
	}
}