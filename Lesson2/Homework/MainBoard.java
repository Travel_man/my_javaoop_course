public class MainBoard {

	public static void main(String[] args) {
		Board board = new Board();
		Circle circle = new Circle(new Point(5, 2), new Point(3, 2));
		Triangle triangle = new Triangle(new Point(5, 2), new Point(3, 2), new Point(4, 0));
		Square square = new Square(new Point(5, 2), new Point(3, 2), new Point(4, 0), new Point(-4, -2));
		System.out.println(circle.toString());
		System.out.println(triangle.toString());
		System.out.println(square.toString());
		board.getShapesInfo();// показ пустой доски
		board.addShape(circle, 2);
		board.addShape(triangle, 1);
		board.addShape(square, 4);
		board.getShapesInfo();

	}

}