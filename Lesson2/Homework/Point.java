public class Point {

	private double x;
	private double y;

	public Point(double x, double y) {
		super();
		this.x = x;
		this.y = y;
	}

	public Point() {
		super();
	}

	public double getX() {
		return x;
	}

	public void setX(double x) {
		this.x = x;
	}

	public double getY() {
		return y;
	}

	public void setY(double y) {
		this.y = y;
	}

	public double calcDistance(Point point) {
		double a = this.x - point.getX();
		double b = this.y - point.getY();
		return Math.sqrt(a * a + b * b);

	}

}