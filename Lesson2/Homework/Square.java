public class Square extends Shape {

	private Point a;
	private Point b;
	private Point c;
	private Point d;

	public Square(Point a, Point b, Point c, Point d) {
		super();
		this.a = a;
		this.b = b;
		this.c = c;
		this.d = d;
	}

	public Square() {

	}

	public Point getA() {
		return a;
	}

	public void setA(Point a) {
		this.a = a;
	}

	public Point getB() {
		return b;
	}

	public void setB(Point b) {
		this.b = b;
	}

	public Point getC() {
		return c;
	}

	public void setC(Point c) {
		this.c = c;
	}

	public Point getD() {
		return d;
	}

	public void setD(Point d) {
		this.d = d;
	}

	private double getSide(Point a, Point b) {
		return Math.sqrt(b.calcDistance(a));
	}

	@Override
	double getPerimetr() {
		return 2 * getSide(a, b) + 2 * getSide(c, d);
	}

	@Override
	double getArea() {

		return getSide(a, b) * getSide(c, d);
	}

	@Override
	public String toString() {
		return "Square [getPerimetr()=" + getPerimetr() + ", getArea()=" + getArea() + "]";
	}

}