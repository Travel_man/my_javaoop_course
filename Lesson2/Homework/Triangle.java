public class Triangle extends Shape {

	private Point a;
	private Point b;
	private Point c;

	public Triangle(Point a, Point b, Point c) {
		super();
		this.a = a;
		this.b = b;
		this.c = c;
	}

	public Triangle() {

	}

	public Point getA() {
		return a;
	}

	public void setA(Point a) {
		this.a = a;
	}

	public Point getB() {
		return b;
	}

	public void setB(Point b) {
		this.b = b;
	}

	public Point getC() {
		return c;
	}

	public void setC(Point c) {
		this.c = c;
	}

	private double getSide(Point a, Point b) {
		return Math.sqrt(b.calcDistance(a));
	}

	@Override
	public double getPerimetr() {

		return getSide(a, b) + getSide(b, c) + getSide(c, a);
	}

	@Override
	public double getArea() {
		double p = getPerimetr() / 2;
		return Math.sqrt(p * (p - getSide(a, b)) * (p - getSide(b, c)) * (p - getSide(c, a)));
	}

	@Override
	public String toString() {
		return "Triangle [getPerimetr()=" + getPerimetr() + ", getArea()=" + getArea() + "]";
	}
}
