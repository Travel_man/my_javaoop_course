public class EmptyGroupException extends Exception {

	@Override
	public String getMessage() {

		return " Warning! Group is empty.";
	}

}
