import java.util.Arrays;
public class Group {

	private Student student;
	private int listNumber = 0;
	private Student[] group = new Student[10];

	public Group(Student student) {

	}

	public Group() {

	}

	public Student getStudent() {
		return student;
	}

	public void setStudent(Student student) {
		this.student = student;
	}

	public int getListNumber() {
		return listNumber;
	}

	public void setListNumber(int listNumber) {
		this.listNumber = listNumber;
	}

	public void addStudent(Student student) throws myException {
		try {
			for (int i = 0; i < group.length; i++) {
				if (group[i] == null) {
					group[i] = student;
					listNumber++;
					break;
				}
				if (i == 9)
					throw new myException();
			}
		} catch (myException e) {
			System.out.println(e.getMessage());
		}
	}

	public void delStudent(Student student) throws EmptyGroupException {
		try {
			for (int i = 0; i < group.length;) {
				if (group[i] == student)
					group[i] = null;
				listNumber--;
				break;
			}
			if (listNumber == 0) {
				throw new EmptyGroupException();
			}
		}

		catch (EmptyGroupException e) {
			System.out.println(e.getMessage());
		}

	}

	public void findStudent(Student student) throws EmptyGroupException {
		try {
			for (int i = 0; i < group.length;) {
				if (student.equals(group[i])) {
					System.out.println(student.getSurname());
					break;
				} else {
					System.out.println(" This student can not find in this group.");
				}
				throw new EmptyGroupException();
			}
		}

		catch (EmptyGroupException e) {
			System.out.println(e.getMessage());
		}

	}

	@Override
	public String toString() {

		try {
			Arrays.sort(group);

			throw new EmptyGroupException();
		} catch (EmptyGroupException e) {
			System.out.println(e.getMessage());
		}

		return "Group [student=" + student + ", group=" + Arrays.toString(group) + ", toString()=" + super.toString()
				+ "]";
	}
}
