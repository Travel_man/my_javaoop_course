public class MainPerson {

	public static void main(String[] args) {

		Person Vasya = new Person("Vasiliy", "Gavryliuk", 50, 88);
		System.out.println(Vasya.getInformaion());
			
		Student student1 = new Student("Sergiy", "Gavryliuk", 20, 150, 12);
		Student student2 = new Student("Sergiyko", "Gavryliuka", 20, 150, 12);
		Student student3 = new Student("Petro", "Khromiy", 20, 88, 12);
		Student student4 = new Student("Vasyl", "Fishman", 21, 90, 12);
		Student student5 = new Student("Anton", "Petlura", 19, 78, 12);
		Student student6 = new Student("Yulia", "Timoshenko", 22, 65, 12);
		Student student7 = new Student("Oxana", "Green", 18, 58, 12);
		Student student8 = new Student("Mykola", "Pershyy", 19, 72, 12);
		Student student9 = new Student("Olesya", "Bila", 19, 62, 12);
		Student student10 = new Student("Lena", "Prekrasnaya", 20, 58, 12);
		Student student11 = new Student("Vika", "Zabiyaka", 18, 60, 13);

		Group group = new Group();
		group.addStudent(student1);
		group.addStudent(student2);
		group.addStudent(student3);
		group.addStudent(student4);
		group.addStudent(student5);
		group.addStudent(student6);
		group.addStudent(student7);
		group.addStudent(student8);
		group.addStudent(student9);
		group.addStudent(student10);
		System.out.println(group);

		group.addStudent(student11);
		System.out.println(group);

		group.findStudent(student9);
		group.delStudent(student7);

		System.out.println(group);

	}

}