
public class Person {

	private String name;
	private String surname;
	private int age;
	private double weight;
	
	
	Person (String name, String surname, int age, double weight){
	super();
		this.name = name;
		this.surname = surname;
		this.age = age;
		this.weight = weight;
		
	}
	
	
	Person (){
		super();
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getSurname() {
		return surname;
	}


	public void setSurname(String surname) {
		this.surname = surname;
	}


	public int getAge() {
		return age;
	}


	public void setAge(int age) {
		this.age = age;
	}


	public double getWeight() {
		return weight;
	}


	public void setWeight(double weight) {
		this.weight = weight;
	}

		public String getInformaion() {
		return(" surname=" + surname + ", name=" + name + ", age=" + age + ", weight=" + weight );
	}

}