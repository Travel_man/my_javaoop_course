public class Student extends Person {
	private int groupNumber;
	private Group group;

	public Student(String name, String surname, int age, double weight, int groupNumber) {
		super(name, surname, age, weight);
		this.groupNumber = groupNumber;

	}

	public Student() {
		super();
	}

	public int getGroupNumber() {
		return groupNumber;
	}

	public void setGroupNumber(int groupNumber) {
		this.groupNumber = groupNumber;
	}

	public Group getGroup() {
		return group;
	}

	public void setGroup(Group group) {
		this.group = group;
	}

	@Override
	public String getInformaion() {

		return (" surname=" + getSurname() + ", name=" + getName() + ", groupNumber=" + groupNumber);
	}
}